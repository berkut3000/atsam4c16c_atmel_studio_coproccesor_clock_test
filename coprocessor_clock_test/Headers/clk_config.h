/*
 * clk_config.h
 *
 * Created: 18/09/2017 06:49:08 p.m.
 *  Author: hydro
 */ 

#ifndef CLK_CONFIG_H_
#define CLK_CONFIG_H_

#include "sam.h"

void clock_init();

#endif /* CLK_CONFIG_H_ */